import os
make = "make"
mc = "make clean"
sleep5 = "sleep 5"
sleep1 = "sleep 1"
lines = "echo ------------------- >> out_cuda_shared.txt"

os.system(mc)
os.system(make)

os.system(sleep5)

ns = [16, 64, 128, 256, 512, 1024, 2048]
# ns = [16]

i = 0
for n in ns:
	print("running for " + str(n))

	for t in range(5):
		run = "./shared2 " + str(n) + " >> out_cuda_shared.txt"
		os.system(run)

		os.system(sleep1)

		os.system(lines)

	i = i + 1
	os.system(lines)
	os.system(lines)