import os
make = "make"
mc = "make clean"
sleep5 = "sleep 5"
sleep1 = "sleep 1"
lines = "echo ------------------- >> out_mpiSync_differentN.txt"

os.system(mc)
os.system(make)
os.system("echo nodes=1, processors=4 >> out_mpiSync_differentN.txt")

os.system(sleep5)

ns = [16, 64, 128, 256, 512, 1024, 2048, 4096, 16384, 32768, 65536, 131072]
# ns = [16]

i = 0
for n in ns:
	print("running for " + str(n))

	for t in range(5):
		run = "mpirun -n 4 ./mpi_sync2 " + str(n) + " >> out_mpiSync_differentN.txt"
		os.system(run)

		if i < 10:
			os.system(sleep1)
		else:
			os.system(sleep5)

		os.system(lines)

	i = i + 1
	os.system(lines)
	os.system(lines)