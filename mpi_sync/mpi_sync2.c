#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <mpi.h>

#include "city.h"
#include "tour.h"

#define BILLION  1000000000L;

// for MPI_Allreduce to get rank
struct glob { 
        double val; 
        int   rank; 
    }; 


void printArray (int *arr, int n) { 
	int i;
    for (i = 0; i < n; i++)  {
        printf("%i ", arr[i]); 
    }
    printf("\n"); 
} 



double anneal(struct city *all_cities, int *curr_tour, int n, int myrank) {

// init

	int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	double temperature = 0.05*(double)n/(double)(world_size*0.5);
	if (temperature < 1) {
		temperature = 5;
	}
	int *next_tour = (int *) malloc(n*sizeof(int));
	double new_cost;

	int *temp;
	double prob;
	double random_prob;

	int epochs = 0;
	int freq = n/1000;

	if (freq < 10) {
		freq = 10;
	}

	int *original = next_tour; 

// get cost of current tour
	double curr_cost = tour_cost(all_cities, curr_tour, n);


	while (temperature > 1) {


	// get neighbour tour
		new_cost = generate_neighbour_tour_and_cost(all_cities, next_tour, curr_tour, n, curr_cost);


		if (new_cost < curr_cost) {
		// new tour becomes current tour
		// swap pointers
			temp = curr_tour;
			curr_tour = next_tour;
			next_tour = temp;
			curr_cost = new_cost;
		} else {
		// compute probability of accepting worse tour
			prob = exp((curr_cost-new_cost)/temperature);
			random_prob = (double)rand()/RAND_MAX;

        	if (prob > random_prob) {
        		temp = curr_tour;
				curr_tour = next_tour;
				next_tour = temp;
				curr_cost = new_cost;
        	}
		}

		temperature = 0.999*temperature;
		epochs++;
		// printf("\n\n");

		if (epochs % freq == 0) {
		//reduce and give everyone the same tour

			MPI_Barrier(MPI_COMM_WORLD);

			struct glob global_anneal;
			struct glob local_anneal;

			local_anneal.val = curr_cost;
			local_anneal.rank = myrank;
			MPI_Allreduce(&local_anneal, &global_anneal, 1, MPI_DOUBLE_INT, MPI_MINLOC, MPI_COMM_WORLD);

			curr_cost = global_anneal.val;

			MPI_Bcast(curr_tour, n, MPI_INT, global_anneal.rank, MPI_COMM_WORLD);

		}

	}

	free(original);

	if (myrank == 0) {
		printf("Iterations: %i\n", epochs);
	}


	return curr_cost;

}



void generate_cities(struct city *cities, int n) {
	int i;
	for (i = 0; i < n; i++) {
		// generate random points
		int x = random() % 100;
		int y = random() % 100;

		cities[i].x = x;
		cities[i].y = y;
	}
}



int main(int argc, char **argv) {

// for generating distances between cities
	srandom((time(NULL)));

	int n;
	int myrank, numprocs;
	double final_cost, red_cost;

	if (argc > 1) {
		n = atoi(argv[1]);
	} else {
		printf("need input: number of cities\n");
		return 0;
	}

	// make cities using struct
	struct city *all_cities;
	all_cities = malloc(n*sizeof(struct city));
	generate_cities(all_cities, n);

// generate random initial tour
	int *first_tour = (int *) malloc(n*sizeof(int));
	generate_first_tour(first_tour, n);


// ------------------------- MPI -------------------------------

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

	if (myrank == 0) {
		printf("Problem size: %i\n", n);
	}

	MPI_Barrier(MPI_COMM_WORLD); /* IMPORTANT */
	double start = MPI_Wtime();

// set random seed using process number
	if (myrank > 0) {
		srandom(myrank);
	}
	

	// // simulated annealing
	final_cost = anneal(all_cities, first_tour, n, myrank);
    struct glob globalres;
	struct glob localres;

	localres.val = final_cost;
	localres.rank = myrank;
	MPI_Allreduce(&localres, &globalres, 1, MPI_DOUBLE_INT, MPI_MINLOC, MPI_COMM_WORLD);

	if (myrank == globalres.rank) {
		printf("Final cost: %f, process: %i\n", globalres.val, globalres.rank);
	}


	free(all_cities);
	free(first_tour);


	MPI_Barrier(MPI_COMM_WORLD); /* IMPORTANT */
	double end = MPI_Wtime();

	MPI_Finalize();

	if (myrank == 0) { /* use time on master node */
		printf("Processing time: %f s\n", end-start );
	}


	return 0;

}
