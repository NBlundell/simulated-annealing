#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "city.h"
#include "tour.h"

#define BILLION  1000000000L;


void printArray (int *arr, int n) { 
    int i;
    for (i = 0; i < n; i++)  {
        printf("%i ", arr[i]); 
    }
    printf("\n"); 
} 



double anneal(struct city *all_cities, int *curr_tour, int n) {

// init
	long double temperature = (double)n* (double)n;
	printf("temp= %Lf\n",temperature );
	int *next_tour = (int *) malloc(n*sizeof(int));
	double new_cost;

	int *temp;
	double prob;
	double random_prob;

	int iterations = 0;

	int *original = next_tour; 

// get cost of current tour
	double curr_cost = tour_cost(all_cities, curr_tour, n);

	// printf("initial cost: %f\n", curr_cost);

	while (temperature > 1) {

		// printf("temperature: %f\n", temperature);
		// printf("current cost: %f\n", curr_cost);

	// get neighbour tour
		generate_neighbour_tour(all_cities, next_tour, curr_tour, n);
		// printArray(next_tour,n);

	//cost of neighbour tour
		new_cost = tour_cost(all_cities, next_tour, n);
		// printf("new cost: %f\n", new_cost);

		if (new_cost < curr_cost) {
		// new tour becomes current tour
		// swap pointers
			temp = curr_tour;
			curr_tour = next_tour;
			next_tour = temp;
			curr_cost = new_cost;
		} else {
		// compute probability of accepting worse tour
			prob = exp((curr_cost-new_cost)/temperature);
			random_prob = (double)rand()/RAND_MAX;
        	// printf("e: %f, p: %f\n",prob,random_prob);

        	if (prob > random_prob) {
        		temp = curr_tour;
				curr_tour = next_tour;
				next_tour = temp;
				curr_cost = new_cost;
        	}
		}

		temperature = 0.999*temperature;
		iterations++;
		// printf("\n\n");

	}

	free(original);

	printf("iterations: %i\n", iterations);

	return curr_cost;

}



void generate_cities(struct city *cities, int n) {
		int i;
	for ( i = 0; i < n; i++) {
		// generate random points
		int x = random() % 100;
		int y = random() % 100;

		cities[i].x = x;
		cities[i].y = y;
		// printf("city number %i has x: %i and y: %i\n", i, getX(cities[i]), getY(cities[i]));
	}
}



int main(int argc, char **argv) {

	srandom((time(NULL)));

// for calculating time
	double time;
	struct timespec start, end;

	int n;

	if (argc > 1) {
		n = atoi(argv[1]);
	} else {
		printf("need input: number of cities\n");
		return 0;
	}


	// make cities using struct
	struct city *all_cities;
	all_cities = malloc(n*sizeof(struct city));
	generate_cities(all_cities, n);
	// printf("distance test: %f\n", distance(all_cities[0], all_cities[1]));

	// generate random initial tour
	int *first_tour = (int *) malloc(n*sizeof(int));
	generate_first_tour(first_tour, n);
	// printArray(first_tour, n);

	// simulated annealing
	printf("starting simulated annealing...\n");

	if( clock_gettime(CLOCK_REALTIME, &start) == -1){
      perror( "clock gettime" );
      exit( EXIT_FAILURE );
	}

	double cost = anneal(all_cities, first_tour, n);

	if( clock_gettime(CLOCK_REALTIME, &end) == -1){
      perror( "clock gettime" );
      exit( EXIT_FAILURE );
  	}

	time = (end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec)/BILLION;

	printf("done with simulated annealing\n");
	printf("final cost: %f\n", cost);
	printf("time taken: %lf s\n", time);


	free(all_cities);
	free(first_tour);
	return 0;

}