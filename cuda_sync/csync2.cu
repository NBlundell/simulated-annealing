#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include <cuda_runtime.h>
#include <curand.h>
#include <curand_kernel.h>
#include <helper_cuda.h>	         // helper functions for CUDA error check
#include "helper_functions.h"    // includes cuda.h and cuda_runtime_api.h
#include "inc/cub/cub.cuh"


#define BILLION  1000000000L;

// Global variable for timer functions
StopWatchInterface *timer = NULL;



struct city
{
	int x;
	int y;

};




//  ____________________ CUDA ________________________


///////////////##############################////////////////////////////////////////////////////////////////////////
// Kernel Function
// Naive GPU implementation 
/////////////////###########################/////////////////////////////////////////////////////////////////////////
__global__ void sync_anneal(int start, cub::KeyValuePair<int, double>  *d_out, double temperature, struct city *all_cities, int n, int *d_curr, int *d_next, double *d_costs){

	int id = threadIdx.x + blockIdx.x * blockDim.x;

	curandState S;
	curand_init(id,0,0,&S);

	int k;
	if (start == 1) {
		if (id != 0) {
			for (k = 0; k < n; k++) {
				d_curr[n*id + k] = d_curr[k];
			} 
		}

	}else{
		int imin = d_out[0].key;
		for (k = 0; k < n; k++) {
			d_curr[n*id + k] = d_next[n*imin + k];
		}
	}



// initialise variables for annealing
// =====================================================================================

	int iters = 0;
	int *temp3;
	double new_cost;

	double prob;
	double random_prob;

// =======================================================================================


// get cost of current tour
// =====================================================================================

	double curr_cost = 0;
	int a;
	for ( a = 0; a < n-1; a++) {
		curr_cost = curr_cost + sqrtf( (all_cities[d_curr[n+id + a]].x - all_cities[d_curr[n+id + a+1]].x )*(all_cities[d_curr[n+id + a]].x - all_cities[d_curr[n+id + a+1]].x )
			+ (all_cities[d_curr[n+id + a]].y - all_cities[d_curr[n+id + a+1]].y )*(all_cities[d_curr[n+id + a]].y - all_cities[d_curr[n+id + a+1]].y ) );
	}
	curr_cost = curr_cost + sqrtf( (all_cities[d_curr[n+id + n-1]].x - all_cities[d_curr[n+id + 0]].x )*(all_cities[d_curr[n+id + n-1]].x - all_cities[d_curr[n+id + 0]].x )
			+ (all_cities[d_curr[n+id + n-1]].y - all_cities[d_curr[n+id + 0]].y )*(all_cities[d_curr[n+id + n-1]].y - all_cities[d_curr[n+id + 0]].y ) );

// =====================================================================================

// start annealing loop
	while (iters < 10) {

// get neighbour tour
// =====================================================================================
		int k;
		for (k = 0; k < n; k++) {
			d_next[n*id + k] = d_curr[n*id + k];
		}  

		int c1 = curand_uniform(&S) * (n);
        int c2 = curand_uniform(&S) * (n);

        while(c2==c1){
	        c2 = curand_uniform(&S) * (n);
        }

	    int temp2;
   		temp2 = d_next[n*id+c1];
   		d_next[n*id+c1] = d_next[n*id + c2];
   		d_next[n*id + c2] = temp2;
// =====================================================================================


// get cost of neighbour tour
// =====================================================================================
   		int ind1 = n*id + c1;
   		int ind2 = n*id + c2;
		new_cost = curr_cost;


		// subtracting old costs
		// c1 with previous
		if (c1 == 0) {
			new_cost = new_cost - sqrtf( (all_cities[d_curr[ind1]].x - all_cities[d_curr[n-1]].x )*(all_cities[d_curr[ind1]].x - all_cities[d_curr[n-1]].x )
			+ (all_cities[d_curr[ind1]].y - all_cities[d_curr[n-1]].y )*(all_cities[d_curr[ind1]].y - all_cities[d_curr[n-1]].y ) );
		}else {
			new_cost = new_cost - sqrtf( (all_cities[d_curr[ind1]].x - all_cities[d_curr[ind1-1]].x )*(all_cities[d_curr[ind1]].x - all_cities[d_curr[ind1-1]].x )
			+ (all_cities[d_curr[ind1]].y - all_cities[d_curr[ind1-1]].y )*(all_cities[d_curr[ind1]].y - all_cities[d_curr[ind1-1]].y ) );
		}

		// c1 with next
		if (c1 == n-1) {
			new_cost = new_cost - sqrtf( (all_cities[d_curr[ind1]].x - all_cities[d_curr[0]].x )*(all_cities[d_curr[ind1]].x - all_cities[d_curr[0]].x )
			+ (all_cities[d_curr[ind1]].y - all_cities[d_curr[0]].y )*(all_cities[d_curr[ind1]].y - all_cities[d_curr[0]].y ) );
		} else{
			new_cost = new_cost - sqrtf( (all_cities[d_curr[ind1]].x - all_cities[d_curr[ind1+1]].x )*(all_cities[d_curr[ind1]].x - all_cities[d_curr[ind1+1]].x )
			+ (all_cities[d_curr[ind1]].y - all_cities[d_curr[ind1+1]].y )*(all_cities[d_curr[ind1]].y - all_cities[d_curr[ind1+1]].y ) );
		}

		// c2 with previous
		if (c2 == 0) {
			new_cost = new_cost - sqrtf( (all_cities[d_curr[ind2]].x - all_cities[d_curr[n-1]].x )*(all_cities[d_curr[ind2]].x - all_cities[d_curr[n-1]].x )
			+ (all_cities[d_curr[ind2]].y - all_cities[d_curr[n-1]].y )*(all_cities[d_curr[ind2]].y - all_cities[d_curr[n-1]].y ) );
		}else {
			new_cost = new_cost - sqrtf( (all_cities[d_curr[ind2]].x - all_cities[d_curr[ind2-1]].x )*(all_cities[d_curr[ind2]].x - all_cities[d_curr[ind2-1]].x )
			+ (all_cities[d_curr[ind2]].y - all_cities[d_curr[ind2-1]].y )*(all_cities[d_curr[ind2]].y - all_cities[d_curr[ind2-1]].y ) );
		}

		// c2 with next
		if (c2 == n-1) {
			new_cost = new_cost - sqrtf( (all_cities[d_curr[ind2]].x - all_cities[d_curr[0]].x )*(all_cities[d_curr[ind2]].x - all_cities[d_curr[0]].x )
			+ (all_cities[d_curr[ind2]].y - all_cities[d_curr[0]].y )*(all_cities[d_curr[ind2]].y - all_cities[d_curr[0]].y ) );
		} else{
			new_cost = new_cost - sqrtf( (all_cities[d_curr[ind2]].x - all_cities[d_curr[ind2+1]].x )*(all_cities[d_curr[ind2]].x - all_cities[d_curr[ind2+1]].x )
			+ (all_cities[d_curr[ind2]].y - all_cities[d_curr[ind2+1]].y )*(all_cities[d_curr[ind2]].y - all_cities[d_curr[ind2+1]].y ) );
		}


		// adding new costs
		// c1 with previous
		if (c1 == 0) {
			new_cost = new_cost + sqrtf( (all_cities[d_next[ind1]].x - all_cities[d_next[n-1]].x )*(all_cities[d_next[ind1]].x - all_cities[d_next[n-1]].x )
			+ (all_cities[d_next[ind1]].y - all_cities[d_next[n-1]].y )*(all_cities[d_next[ind1]].y - all_cities[d_next[n-1]].y ) );
		}else {
			new_cost = new_cost + sqrtf( (all_cities[d_next[ind1]].x - all_cities[d_next[ind1-1]].x )*(all_cities[d_next[ind1]].x - all_cities[d_next[ind1-1]].x )
			+ (all_cities[d_next[ind1]].y - all_cities[d_next[ind1-1]].y )*(all_cities[d_next[ind1]].y - all_cities[d_next[ind1-1]].y ) );
		}

		// c1 with next
		if (c1 == n-1) {
			new_cost = new_cost + sqrtf( (all_cities[d_next[ind1]].x - all_cities[d_next[0]].x )*(all_cities[d_next[ind1]].x - all_cities[d_next[0]].x )
			+ (all_cities[d_next[ind1]].y - all_cities[d_next[0]].y )*(all_cities[d_next[ind1]].y - all_cities[d_next[0]].y ) );
		} else{
			new_cost = new_cost + sqrtf( (all_cities[d_next[ind1]].x - all_cities[d_next[ind1+1]].x )*(all_cities[d_next[ind1]].x - all_cities[d_next[ind1+1]].x )
			+ (all_cities[d_next[ind1]].y - all_cities[d_next[ind1+1]].y )*(all_cities[d_next[ind1]].y - all_cities[d_next[ind1+1]].y ) );
		}

		// c2 with previous
		if (c2 == 0) {
			new_cost = new_cost + sqrtf( (all_cities[d_next[ind2]].x - all_cities[d_next[n-1]].x )*(all_cities[d_next[ind2]].x - all_cities[d_next[n-1]].x )
			+ (all_cities[d_next[ind2]].y - all_cities[d_next[n-1]].y )*(all_cities[d_next[ind2]].y - all_cities[d_next[n-1]].y ) );
		}else {
			new_cost = new_cost + sqrtf( (all_cities[d_next[ind2]].x - all_cities[d_next[ind2-1]].x )*(all_cities[d_next[ind2]].x - all_cities[d_next[ind2-1]].x )
			+ (all_cities[d_next[ind2]].y - all_cities[d_next[ind2-1]].y )*(all_cities[d_next[ind2]].y - all_cities[d_next[ind2-1]].y ) );
		}

		// c2 with next
		if (c2 == n-1) {
			new_cost = new_cost + sqrtf( (all_cities[d_next[ind2]].x - all_cities[d_next[0]].x )*(all_cities[d_next[ind2]].x - all_cities[d_next[0]].x )
			+ (all_cities[d_next[ind2]].y - all_cities[d_next[0]].y )*(all_cities[d_next[ind2]].y - all_cities[d_next[0]].y ) );
		} else{
			new_cost = new_cost + sqrtf( (all_cities[d_next[ind2]].x - all_cities[d_next[ind2+1]].x )*(all_cities[d_next[ind2]].x - all_cities[d_next[ind2+1]].x )
			+ (all_cities[d_next[ind2]].y - all_cities[d_next[ind2+1]].y )*(all_cities[d_next[ind2]].y - all_cities[d_next[ind2+1]].y ) );
		}

// =====================================================================================


		if (new_cost < curr_cost) {
		// new tour becomes current tour
		// swap pointers
			temp3 = d_curr;
			d_curr = d_next;
			d_next = temp3;
			curr_cost = new_cost;
		} else {
		// compute probability of accepting worse tour
			prob = exp((curr_cost-new_cost)/temperature);
			random_prob = curand_uniform(&S);

        	if (prob > random_prob) {
        		temp3 = d_curr;
				d_curr = d_next;
				d_next = temp3;
				curr_cost = new_cost;
        	}
		}

		temperature = 0.999*temperature;
		iters++;
	}
// =====================================================================================

	d_costs[id] = curr_cost;

	__syncthreads();


}









































void printArray (int *arr, int n) { 
    int i;
    for (i = 0; i < n; i++)  {
        printf("%i ", arr[i]); 
    }
    printf("\n"); 
} 



void h_swap (int *a, int *b) { 
    int temp = *a; 
    *a = *b; 
    *b = temp; 
} 


void generate_first_tour(int *tour, int n) {

// init array
	int k;
	for (k = 0; k < n; k++) {
		tour[k] = k;
	}  

   // swap elements starting from end
    int i;
    for (i = n-1; i > 0; i--) { 
        // int j = random() % (i+1); 
   		int j = random() % (i+1);

        h_swap(&tour[i], &tour[j]); 
    } 

}




void generate_cities(struct city *cities, int n) {
		int i;
	for ( i = 0; i < n; i++) {
		// generate random points
		int x = random() % 100;
		int y = random() % 100;

		cities[i].x = x;
		cities[i].y = y;
		// printf("city number %i has x: %i and y: %i\n", i, getX(cities[i]), getY(cities[i]));
	}
}




int main(int argc, char **argv) {

	srandom((time(NULL)));


	int n;

	if (argc > 1) {
		n = atoi(argv[1]);
	} else {
		printf("need input: number of cities\n");
		return 0;
	}

	printf("Problem size: %i cities\n", n);


	// make cities using struct
	struct city *all_cities;
	all_cities = (struct city*) malloc(n*sizeof(struct city));
	generate_cities(all_cities, n);

	// generate random initial tour
	int *first_tour = (int *) malloc(n*sizeof(int));
	generate_first_tour(first_tour, n);


////////////////////// Allocate device mem and copy data //////////////////////////
	int num_threads = 32;
	int num_blocks = 112;

    struct city *d_cities;      // input data on the device
    double *d_costs;
    int *d_curr;
    int *d_next;



    
    checkCudaErrors(cudaMalloc((void **)&d_cities, n*sizeof(struct city)));
    checkCudaErrors(cudaMalloc((void **)&d_costs, num_threads*num_blocks*sizeof(double) ) );
    checkCudaErrors(cudaMalloc((void **)&d_curr, num_threads*num_blocks*n*sizeof(int) ) );
    checkCudaErrors(cudaMalloc((void **)&d_next, num_threads*num_blocks*n*sizeof(int) ) );
    checkCudaErrors(cudaMemcpy(d_cities, all_cities, n*sizeof(struct city), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_curr, first_tour, n*sizeof(int), cudaMemcpyHostToDevice));


    cub::KeyValuePair<int, double>  *d_out;
    CubDebugExit(cudaMalloc((void**)&d_out, sizeof(cub::KeyValuePair<int, double>)));

// define kernel config
    dim3 blk_size(num_threads);
    dim3 grid_size(num_blocks);




    double temperature = 0.0025*n;
    int iters = 0;
    int start = 1;

    if (temperature < 1) {
    	temperature = 5;
    }

//  Synchronous loop:

    checkCudaErrors(cudaDeviceSynchronize());
  	sdkCreateTimer(&timer);
  	sdkStartTimer(&timer);

	while (temperature > 1) {


	// call kernel //////////////////////////////////////////////////////////////////////////////////////////
	    sync_anneal<<<grid_size, blk_size>>>(start, d_out, temperature, d_cities, n, d_curr, d_next, d_costs);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////


		cudaDeviceSynchronize(); //wait for all anealing to be done

	/////////////////////////////Reduction////////////////////////////////////////////////////
		
		// Determine temporary device storage requirements
		void     *d_temp_storage = NULL;
		size_t   temp_storage_bytes = 0;
		cub::DeviceReduce::ArgMin(d_temp_storage, temp_storage_bytes, d_costs, d_out, num_threads*num_blocks);


		// Allocate temporary storage
		CubDebugExit(cudaMalloc(&d_temp_storage, temp_storage_bytes));


		// Run min-reduction
		cub::DeviceReduce::ArgMin(d_temp_storage, temp_storage_bytes, d_costs, d_out, num_threads*num_blocks);	

	//////////////////////////////////////////////////////////////////////////////////////////

		temperature = 0.99*temperature;
		iters = iters + 10;
		start = 0;

	}




// verify if exec'd
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) 
    printf("Error: %s\n", cudaGetErrorString(err));

    checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&timer);



// copy result to host
	cub::KeyValuePair<int, double> *final_cost = (cub::KeyValuePair<int, double> *) malloc(sizeof(cub::KeyValuePair<int, double>));
	// copy reduction to host
	checkCudaErrors(cudaMemcpy(final_cost, d_out, sizeof(cub::KeyValuePair<int, double>), cudaMemcpyDeviceToHost));

	printf("Iterations: %i\n", iters);
	printf("Final cost: %f\n", final_cost[0].value );
	printf("Processing time: %f s\n", sdkGetTimerValue(&timer)/1000.0);

    checkCudaErrors(cudaFree(d_cities));
    checkCudaErrors(cudaFree(d_costs));
    checkCudaErrors(cudaFree(d_curr));
    checkCudaErrors(cudaFree(d_next));

	free(all_cities);

	cudaDeviceReset();
	return 0;

}
