// #include "city.h"

double tour_cost(struct city *cities, int *tour, int n);
void generate_first_tour(int *tour, int n);
double generate_neighbour_tour_and_cost(struct city *cities, int *next_tour, int *curr_tour, int n, double old_cost);