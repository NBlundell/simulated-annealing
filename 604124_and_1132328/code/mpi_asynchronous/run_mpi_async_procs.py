import os
import math
make = "make"
mc = "make clean"
sleep5 = "sleep 5"
sleep1 = "sleep 1"
lines = "echo ------------------- >> out_mpiasync_differentP.txt"

os.system(mc)
os.system(make)

np = [2,4,8,16,32,64]
# np = [2]

hosts = ["mscluster23", "mscluster24", "mscluster25", "mscluster26", "mscluster27", "mscluster28", "mscluster30", "mscluster31",
		 "mscluster33", "mscluster34", "mscluster35", "mscluster36", "mscluster37", "mscluster38", "mscluster39", "mscluster40"]

i = 0
for n in np:

	r = int(math.ceil(n/4))
	if (r < 1):
		r = 1

	s = ",".join(hosts[:r])

	print("running for " + str(n) + " processors on " + str(r) + " nodes")
	os.system("echo processors = " + str(n) + ", nodes = " + str(r) + " >> out_mpiasync_differentP.txt")

	for t in range(5):
		run = "mpirun -n " + str(n) + " -hosts " + s + " ./mpi_async2 32768 >> out_mpiasync_differentP.txt"
		# print(run)

		os.system(run)

		# if i < 10:
		# 	os.system(sleep1)
		# else:
		# 	os.system(sleep5)

		os.system(lines)

	i = i + 1
	os.system(lines)
	os.system(lines)