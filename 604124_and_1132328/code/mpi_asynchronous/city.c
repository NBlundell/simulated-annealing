
#include <stdlib.h>
#include <math.h>

#include "city.h"


double distance(struct city c1, struct city c2) {
	double d1 = c1.x - c2.x;
	double d2 = c1.y - c2.y;
	return sqrt(d1*d1 + d2*d2);
}