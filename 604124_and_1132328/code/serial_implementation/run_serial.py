import os
make = "make"
mc = "make clean"
sleep5 = "sleep 5"
sleep1 = "sleep 1"

os.system(mc)
os.system(make)

ns = [16, 64, 128, 256, 512, 1024, 2048, 4096, 16384, 32768, 65536, 131072]
# ns = [16, 64, 128, 256]

i = 0
for n in ns:
	print("running for " + str(n))

	for t in range(5):
		run = "./serial2 " + str(n) + " >> out_serial.txt"
		os.system(run)

		if i < 9:
			os.system(sleep1)
		else:
			os.system(sleep5)

		os.system("echo ------------------- >> out_serial.txt")

	i = i + 1
	os.system("echo ------------------- >> out_serial.txt")
	os.system("echo ------------------- >> out_serial.txt")