#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <mpi.h>

#include "city.h"
#include "tour.h"

#define BILLION  1000000000L;

// for MPI_Allreduce to get rank
struct glob { 
        double val; 
        int   rank; 
    }; 


void printArray (int *arr, int n) { 
	int i;
    for (i = 0; i < n; i++)  {
        printf("%i ", arr[i]); 
    }
    printf("\n"); 
} 



double anneal(struct city *all_cities, int *curr_tour, int n, int myrank) {

// init
	long double temperature = (double)n;
	int iters = 0;
	int *next_tour = (int *) malloc(n*sizeof(int));
	double new_cost;

	int *temp;
	double prob;
	double random_prob;

	int *original = next_tour; 

// get cost of current tour
	double curr_cost = tour_cost(all_cities, curr_tour, n);

	// printf("initial cost: %f\n", curr_cost);

	while (temperature > 1) {

		// printf("temperature: %f\n", temperature);
		// printf("current cost: %f\n", curr_cost);

	// get neighbour tour
		generate_neighbour_tour(all_cities, next_tour, curr_tour, n);
		// printArray(next_tour,n);

	//cost of neighbour tour
		new_cost = tour_cost(all_cities, next_tour, n);
		// printf("new cost: %f\n", new_cost);

		if (new_cost < curr_cost) {
		// new tour becomes current tour
		// swap pointers
			temp = curr_tour;
			curr_tour = next_tour;
			next_tour = temp;
			curr_cost = new_cost;
		} else {
		// compute probability of accepting worse tour
			prob = exp((curr_cost-new_cost)/temperature);
			random_prob = (double)rand()/RAND_MAX;
        	// printf("e: %f, p: %f\n",prob,random_prob);

        	if (prob > random_prob) {
        		temp = curr_tour;
				curr_tour = next_tour;
				next_tour = temp;
				curr_cost = new_cost;
        	}
		}

		temperature = 0.999*temperature;
		iters++;
		// printf("\n\n");

	}

	free(original);

	// printf("final cost for process %i: %f\n", myrank, curr_cost);
	if (myrank == 0) {
		printf("iterations: %i\n", iters);
	}

	return curr_cost;

}



void generate_cities(struct city *cities, int n) {
	int i;
	for (i = 0; i < n; i++) {
		// generate random points
		int x = random() % 100;
		int y = random() % 100;

		cities[i].x = x;
		cities[i].y = y;
		// printf("city number %i has x: %i and y: %i\n", i, getX(cities[i]), getY(cities[i]));
	}
}



int main(int argc, char **argv) {

// for generating distances between cities
	srandom((time(NULL)));

	int n;
	int myrank, numprocs;
	double final_cost, red_cost;

	if (argc > 1) {
		n = atoi(argv[1]);
	} else {
		printf("need input: number of cities\n");
		return 0;
	}


	// make cities using struct
	struct city *all_cities;
	all_cities = malloc(n*sizeof(struct city));
	generate_cities(all_cities, n);



// ------------------------- MPI -------------------------------

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

	MPI_Barrier(MPI_COMM_WORLD); /* IMPORTANT */
	double start = MPI_Wtime();

// set random seed using process number
	if (myrank > 0) {
		srandom(myrank);
	}

	// generate random initial tour
	int *first_tour = (int *) malloc(n*sizeof(int));
	generate_first_tour(first_tour, n);
	// printf("process number: %i\n", myrank);
	// printArray(first_tour, n);
	

	// // simulated annealing
	final_cost = anneal(all_cities, first_tour, n, myrank);
    struct glob globalres;
	struct glob localres;

	localres.val = final_cost;
	localres.rank = myrank;
	MPI_Allreduce(&localres, &globalres, 1, MPI_DOUBLE_INT, MPI_MINLOC, MPI_COMM_WORLD);

	if (myrank == globalres.rank) {
		printf("reduced cost: %f, process: %i\n", globalres.val, globalres.rank);
		// printArray(first_tour, n);
	}

	free(all_cities);
	free(first_tour);


	MPI_Barrier(MPI_COMM_WORLD); /* IMPORTANT */
	double end = MPI_Wtime();

	MPI_Finalize();

	if (myrank == 0) { /* use time on master node */
		printf("MPI runtime: %f seconds\n", end-start );
	}


	return 0;

}