#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "city.h"
#include "tour.h"


void swap (int *a, int *b) { 
    int temp = *a; 
    *a = *b; 
    *b = temp; 
} 


double partial_cost(struct city *cities, int *old_tour, int *new_tour, int n, int c1, int c2, double c_cost) {

	double new_cost = c_cost;


// c1 with previous
	if (c1 == 0) {
		new_cost = new_cost - distance(cities[old_tour[c1]], cities[old_tour[n-1]]);
		new_cost = new_cost + distance(cities[new_tour[c1]], cities[new_tour[n-1]]);
	}else {
		new_cost = new_cost - distance(cities[old_tour[c1]], cities[old_tour[c1-1]]);
		new_cost = new_cost + distance(cities[new_tour[c1]], cities[new_tour[c1-1]]);
	}

// c1 with next
	if (c1 == n-1) {
		new_cost = new_cost - distance(cities[old_tour[c1]], cities[old_tour[0]]);
		new_cost = new_cost + distance(cities[new_tour[c1]], cities[new_tour[0]]);
	} else{
		new_cost = new_cost - distance(cities[old_tour[c1]], cities[old_tour[c1+1]]);
		new_cost = new_cost + distance(cities[new_tour[c1]], cities[new_tour[c1+1]]);
	}

// c2 with previous
	if (c2 == 0) {
		new_cost = new_cost - distance(cities[old_tour[c2]], cities[old_tour[n-1]]);
		new_cost = new_cost + distance(cities[new_tour[c2]], cities[new_tour[n-1]]);
	}else {
		new_cost = new_cost - distance(cities[old_tour[c2]], cities[old_tour[c2-1]]);
		new_cost = new_cost + distance(cities[new_tour[c2]], cities[new_tour[c2-1]]);
	}

// c2 with next
	if (c2 == n-1) {
		new_cost = new_cost - distance(cities[old_tour[c2]], cities[old_tour[0]]);
		new_cost = new_cost + distance(cities[new_tour[c2]], cities[new_tour[0]]);
	} else{
		new_cost = new_cost - distance(cities[old_tour[c2]], cities[old_tour[c2+1]]);
		new_cost = new_cost + distance(cities[new_tour[c2]], cities[new_tour[c2+1]]);
	}

	return new_cost;

}










double tour_cost(struct city *cities, int *tour, int n) {
	double sum = 0;
	int i;
	for ( i = 0; i < n; i++) {
		if (i == n-1) {
			sum = sum + distance(cities[tour[i]], cities[tour[0]]);
		} else {
			sum = sum + distance(cities[tour[i]], cities[tour[i+1]]);
		}
	}

	return sum;	
}

void generate_first_tour(int *tour, int n) {

// init array
	int k;
	for (k = 0; k < n; k++) {
		tour[k] = k;
	}  

   // swap elements starting from end
    int i;
    for (i = n-1; i > 0; i--) { 
        int j = random() % (i+1); 
  
        swap(&tour[i], &tour[j]); 
    } 

}


// swap two 
double generate_neighbour_tour_and_cost(struct city *cities, int *next_tour, int *curr_tour, int n, double old_cost) {
	// copy cuurent tour into next_tour
	int k;
	for (k = 0; k < n; k++) {
		next_tour[k] = curr_tour[k];
	}  
// swap two random cities:
    int i = random() % (n); 
    int j = random() % (n); 
    while(j==i){
        j = random() % (n);
    }

    swap(&next_tour[i], &next_tour[j]); 

    double cost = partial_cost(cities, curr_tour, next_tour, n, i, j, old_cost);

    return cost;

}