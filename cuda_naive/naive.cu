#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include <cuda_runtime.h>
#include <curand.h>
#include <curand_kernel.h>
#include <helper_cuda.h>	         // helper functions for CUDA error check
#include "helper_functions.h"    // includes cuda.h and cuda_runtime_api.h
#include <cub/cub.cuh>
// #include "city.h"
// #include "tour.h"

#define BILLION  1000000000L;

// Global variable for timer functions
StopWatchInterface *timer = NULL;



struct city
{
	int x;
	int y;

};




__device__ void swap (int *a, int *b) { 
    int temp = *a; 
    *a = *b; 
    *b = temp; 
} 

__device__ double distance(struct city c1, struct city c2) {
	double d1 = c1.x - c2.x;
	double d2 = c1.y - c2.y;
	return sqrt(d1*d1 + d2*d2);
}


__device__ double tour_cost(int id, struct city *cities, int *tour, int n) {
	double sum = 0;
	int i;
	for ( i = 0; i < n-1; i++) {
		sum = sum + distance(cities[tour[n*id + i]], cities[tour[n*id + i+1]]);
	}
	
	sum = sum + distance(cities[tour[n*id + n-1]], cities[tour[n*id + 0]]);

	return sum;	
}



// swap two 
__device__ void generate_neighbour_tour(int id, int *next_tour, int *curr_tour, int n, curandState S) {
	// copy cuurent tour into next_tour
	int k;
	for (k = 0; k < n; k++) {
		next_tour[n*id + k] = curr_tour[n*id + k];
	}  
// swap two random cities:
        // int i = random() % (n); 
		int i = curand_uniform(&S) * (n);
		// printf("%i\n", i);
        // int j = random() % (n);
        int j = curand_uniform(&S) * (n);
        while(j==i){
	        // j = random() % (n);
	        j = curand_uniform(&S) * (n);
        }

        swap(&next_tour[n*id + i], &next_tour[n*id + j]); 
	
}



__device__ double anneal(int id, struct city *all_cities, int *curr_tour, int *next_tour, int n, curandState S) {

// init

	double temperature = n/8;
	int iters = 0;
	if (temperature < 1) {
		temperature = 5;
	}
	double new_cost;

	int *temp;
	double prob;
	double random_prob;

// get cost of current tour
	double curr_cost = tour_cost(id, all_cities, curr_tour, n);

	// printf("initial cost: %f\n", curr_cost);

	while (temperature > 1) {

		// printf("temperature: %f\n", temperature);
		// printf("current cost: %f\n", curr_cost);

	// get neighbour tour
		generate_neighbour_tour(id, next_tour, curr_tour, n, S);
		// printArray(next_tour,n);

	//cost of neighbour tour
		new_cost = tour_cost(id, all_cities, next_tour, n);
		// printf("new cost: %f\n", new_cost);

		if (new_cost < curr_cost) {
		// new tour becomes current tour
		// swap pointers
			temp = curr_tour;
			curr_tour = next_tour;
			next_tour = temp;
			curr_cost = new_cost;
		} else {
		// compute probability of accepting worse tour
			prob = exp((curr_cost-new_cost)/temperature);
			// random_prob = (double)rand()/RAND_MAX;
			random_prob = curand_uniform(&S);
        	// printf("e: %f, p: %f\n",prob,random_prob);

        	if (prob > random_prob) {
        		temp = curr_tour;
				curr_tour = next_tour;
				next_tour = temp;
				curr_cost = new_cost;
        	}
		}

		temperature = 0.999*temperature;
		iters++;
		// printf("\n\n");

	}

	// printf("final cost: %f\n", curr_cost);
	if (id == 0){
		printf("iterations: %i\n", iters);
	}
	return curr_cost;
}



__device__ void generate_first_tour(int id, int *tour, int n,curandState S) {

// init array
	int k;
	for (k = 0; k < n; k++) {
		tour[n*id + k] = k;
	}  

   // swap elements starting from end
    int i;
    for (i = n-1; i > 0; i--) { 
        // int j = random() % (i+1); 
   		int j = curand_uniform(&S) * (i+1);

        swap(&tour[n*id + i], &tour[n*id + j]); 
    } 

}



//  ____________________ CUDA ________________________


///////////////##############################////////////////////////////////////////////////////////////////////////
// Kernel Function
// Naive GPU implementation 
/////////////////###########################/////////////////////////////////////////////////////////////////////////
__global__ void naive_anneal(struct city *d_cities, int n, int *d_tour, int *d_curr, int *d_next, double *d_costs){

	int id = threadIdx.x + blockIdx.x * blockDim.x;

	// random seed
	// srandom(id);
	curandState randState;
	curand_init(id,0,0,&randState);

	// int i;
	// for (i = 0; i < n; i++) {
	// 	printf("(%i,%i) ", d_cities.x, d_cities.y);
	// }
	// printf("\n");

	// each generate initial tours:
	// int *first_tour = (int *) malloc(n*sizeof(int));

	// =============================================================================
	generate_first_tour(id,d_curr,n,randState);
	// int k;
	// for (k = 0; k < n; k++) {
	// 	d_curr[k] = k;
	// }  
// =====================================================================================



	//anneal
	double cost = anneal(id, d_cities, d_curr, d_next, n, randState);
	d_costs[id] = cost;

	// __syncthreads();


}









































void printArray (int *arr, int n) { 
    int i;
    for (i = 0; i < n; i++)  {
        printf("%i ", arr[i]); 
    }
    printf("\n"); 
} 












void generate_cities(struct city *cities, int n) {
		int i;
	for ( i = 0; i < n; i++) {
		// generate random points
		int x = random() % 100;
		int y = random() % 100;

		cities[i].x = x;
		cities[i].y = y;
		// printf("city number %i has x: %i and y: %i\n", i, getX(cities[i]), getY(cities[i]));
	}
}







int main(int argc, char **argv) {

	srandom((time(NULL)));


	int n;

	if (argc > 1) {
		n = atoi(argv[1]);
	} else {
		printf("need input: number of cities\n");
		return 0;
	}


	// make cities using struct
	struct city *all_cities;
	all_cities = (struct city*) malloc(n*sizeof(struct city));
	generate_cities(all_cities, n);
	// printf("distance test: %f\n", distance(all_cities[0], all_cities[1]));

	// generate random initial tour
	int *final_tour = (int *) malloc(n*sizeof(int));
	// generate_first_tour(first_tour, n);
	// printArray(first_tour, n);

	// simulated annealing
	printf("starting simulated annealing...\n");

	

	// anneal(all_cities, first_tour, n);



////////////////////// Allocate device mem and copy data ?? //////////////////////////
	int num_threads = 32;
	int num_blocks = 20;

    struct city *d_cities;      // input data on the device (copied from hData)
    int *d_tour;   // output data on device
    double *d_costs;
    int *d_curr;
    int *d_next;

	printf("...allocating GPU memory and copying input data\n\n");
    checkCudaErrors(cudaMalloc((void **)&d_cities, n*sizeof(struct city)));
    checkCudaErrors(cudaMalloc((void **)&d_tour, n*sizeof(int) ) );
    checkCudaErrors(cudaMalloc((void **)&d_costs, num_threads*num_blocks*sizeof(double) ) );
    checkCudaErrors(cudaMalloc((void **)&d_curr, num_threads*num_blocks*n*sizeof(int) ) );
    checkCudaErrors(cudaMalloc((void **)&d_next, num_threads*num_blocks*n*sizeof(int) ) );
    checkCudaErrors(cudaMemcpy(d_cities, all_cities, n*sizeof(struct city), cudaMemcpyHostToDevice));


// define kernel config
    dim3 blk_size(num_threads);
    dim3 grid_size(num_blocks);


    checkCudaErrors(cudaDeviceSynchronize());
  	sdkCreateTimer(&timer);
  	sdkStartTimer(&timer);

// call kernel //////////////////////////////////////////////////////////////////////////////////////////
    naive_anneal<<<grid_size, blk_size>>>(d_cities, n, d_tour, d_curr, d_next, d_costs);
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// endTimer(width,height); 


	cudaDeviceSynchronize(); //wait for all anealing to be done

/////////////////////////////Reduction////////////////////////////////////////////////////

	double *d_out = NULL; 
	CubDebugExit(cudaMalloc((void**)&d_out, sizeof(double) * 1));
	// Determine temporary device storage requirements
	void     *d_temp_storage = NULL;
	size_t   temp_storage_bytes = 0;
	cub::DeviceReduce::Min(d_temp_storage, temp_storage_bytes, d_costs, d_out, num_threads*num_blocks);


	// Allocate temporary storage
	CubDebugExit(cudaMalloc(&d_temp_storage, temp_storage_bytes));


	// Run min-reduction
	cub::DeviceReduce::Min(d_temp_storage, temp_storage_bytes, d_costs, d_out, num_threads*num_blocks);	

//////////////////////////////////////////////////////////////////////////////////////////


// verify if exec'd
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) 
    printf("Error: %s\n", cudaGetErrorString(err));

    checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&timer);


	printf("done with simulated annealing\n");

// copy result to host
	double *final_cost = (double *) malloc(1*sizeof(double));
	// copy reduction to host
	checkCudaErrors(cudaMemcpy(final_cost, d_out, sizeof(double), cudaMemcpyDeviceToHost));

	printf("final_cost: %f\n", final_cost[0] );

	checkCudaErrors(cudaMemcpy(final_tour, d_tour, n*sizeof(int), cudaMemcpyDeviceToHost));


	printf("Processing time: %f (ms)\n", sdkGetTimerValue(&timer));

	checkCudaErrors(cudaFree(d_tour));
    checkCudaErrors(cudaFree(d_cities));
    checkCudaErrors(cudaFree(d_costs));
    checkCudaErrors(cudaFree(d_curr));
    checkCudaErrors(cudaFree(d_next));

	free(all_cities);
	free(final_tour);

	cudaDeviceReset();
	return 0;

}