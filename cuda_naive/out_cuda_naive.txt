Problem size: 16 cities
Iterations: 693
Final cost: 320.073009
Processing time: 0.007813 s
-------------------
Problem size: 16 cities
Iterations: 693
Final cost: -23.342670
Processing time: 0.007782 s
-------------------
Problem size: 16 cities
Iterations: 693
Final cost: -237.147421
Processing time: 0.007813 s
-------------------
Problem size: 16 cities
Iterations: 693
Final cost: -167.911781
Processing time: 0.007782 s
-------------------
Problem size: 16 cities
Iterations: 693
Final cost: -494.373551
Processing time: 0.007819 s
-------------------
-------------------
-------------------
Problem size: 64 cities
Iterations: 2079
Final cost: 838.693091
Processing time: 0.061401 s
-------------------
Problem size: 64 cities
Iterations: 2079
Final cost: 1011.736462
Processing time: 0.058820 s
-------------------
Problem size: 64 cities
Iterations: 2079
Final cost: 1027.980365
Processing time: 0.058870 s
-------------------
Problem size: 64 cities
Iterations: 2079
Final cost: 920.725216
Processing time: 0.058123 s
-------------------
Problem size: 64 cities
Iterations: 2079
Final cost: 919.849571
Processing time: 0.058673 s
-------------------
-------------------
-------------------
Problem size: 128 cities
Iterations: 2772
Final cost: 2289.047726
Processing time: 0.143543 s
-------------------
Problem size: 128 cities
Iterations: 2772
Final cost: 2200.196358
Processing time: 0.130162 s
-------------------
Problem size: 128 cities
Iterations: 2772
Final cost: 2159.531767
Processing time: 0.119234 s
-------------------
Problem size: 128 cities
Iterations: 2772
Final cost: 2234.392947
Processing time: 0.119812 s
-------------------
Problem size: 128 cities
Iterations: 2772
Final cost: 2229.405043
Processing time: 0.119391 s
-------------------
-------------------
-------------------
Problem size: 256 cities
Iterations: 3465
Final cost: 5209.772886
Processing time: 0.274358 s
-------------------
Problem size: 256 cities
Iterations: 3465
Final cost: 5134.682104
Processing time: 0.274101 s
-------------------
Problem size: 256 cities
Iterations: 3465
Final cost: 5098.475208
Processing time: 0.275642 s
-------------------
Problem size: 256 cities
Iterations: 3465
Final cost: 5351.248273
Processing time: 0.275869 s
-------------------
Problem size: 256 cities
Iterations: 3465
Final cost: 5273.207943
Processing time: 0.275817 s
-------------------
-------------------
-------------------
Problem size: 512 cities
Iterations: 4157
Final cost: 12502.519063
Processing time: 0.645633 s
-------------------
Problem size: 512 cities
Iterations: 4157
Final cost: 12462.321631
Processing time: 0.645136 s
-------------------
Problem size: 512 cities
Iterations: 4157
Final cost: 12686.126951
Processing time: 0.639970 s
-------------------
Problem size: 512 cities
Iterations: 4157
Final cost: 12400.548890
Processing time: 0.641890 s
-------------------
Problem size: 512 cities
Iterations: 4157
Final cost: 12817.772545
Processing time: 0.639442 s
-------------------
-------------------
-------------------
Problem size: 1024 cities
Iterations: 4850
Final cost: 29929.931747
Processing time: 1.485851 s
-------------------
Problem size: 1024 cities
Iterations: 4850
Final cost: 29976.829958
Processing time: 1.483686 s
-------------------
Problem size: 1024 cities
Iterations: 4850
Final cost: 29518.379061
Processing time: 1.488301 s
-------------------
Problem size: 1024 cities
Iterations: 4850
Final cost: 29826.218564
Processing time: 1.489032 s
-------------------
Problem size: 1024 cities
Iterations: 4850
Final cost: 30265.857288
Processing time: 1.491562 s
-------------------
-------------------
-------------------
Problem size: 2048 cities
Iterations: 5543
Final cost: 69891.794030
Processing time: 3.369072 s
-------------------
Problem size: 2048 cities
Iterations: 5543
Final cost: 69267.766338
Processing time: 3.346781 s
-------------------
Problem size: 2048 cities
Iterations: 5543
Final cost: 69320.310709
Processing time: 3.349798 s
-------------------
Problem size: 2048 cities
Iterations: 5543
Final cost: 68890.142737
Processing time: 3.349919 s
-------------------
Problem size: 2048 cities
Iterations: 5543
Final cost: 69138.825673
Processing time: 3.347374 s
-------------------
-------------------
-------------------
