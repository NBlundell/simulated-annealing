import numpy as np
import re
# import pandas as pd
np.set_printoptions(suppress=True)
import matplotlib.pyplot as plt


def post_processor(input_file, runs_per_experiment, summary=False):
    """Averages runtimes (or other metrics) from input txt file
    for a bunch of different experiments
    input: 'path/to/textfile.txt',
            number of runs per experiment,
            summary (if set to True prints a summary for each experiment),
            
    output:avearges and graphs"""
    
    runs=runs_per_experiment
    
    f= open("%s" % input_file,"r")
    f1 =f.readlines()
    
    
    # Get all processing times
    all_times=[]
    for x in f1:
        if x.startswith('Processing'):
            t=re.findall("\d+\.\d+", x)
            t=float(t[0])
            all_times= np.append(all_times,[t])

    
    
    experiments=all_times.shape[0]/runs
    experiments=int(experiments)
    # print((experiments))
    
    
    # Get every 5 processing times
    sections=all_times.shape[0]/runs
    teepa = np.split(all_times, sections )
    # Get average time for each config:
    all_averages =[]
    for t in teepa:
        avrg = np.average(t)
        all_averages = np.append(all_averages, avrg)



    #  Get all costs:
    all_costs =[]

    for x in f1:
        if x.startswith('Final cost'):
            t=re.findall("\d+\.\d+", x)
            t=float(t[0])
            all_costs= np.append(all_costs,[t])


    #  Get average cost for each config:
    tupsy = np.split(all_costs, sections )
    cost_averages =[]
    for t in tupsy:
        avrg_teepa = np.average(t)
        cost_averages = np.append(cost_averages, avrg_teepa)


    # print(cost_averages)
    
    # Get all iterations
    iterations = []
    for x in f1:
        if x.startswith('Iterations'):
            it=re.findall("\d+", x)
            iterations = np.append(iterations, int(it[0]))

    iterations=iterations[1::5]
#     print((iterations))
    
    
    # Get experiment input size (number of cities)
    problem_sizes = []
    for x in f1:
        if x.startswith('Problem'):
            p=re.findall("\d+", x)
            p=int(p[0])
            problem_sizes = np.append(problem_sizes,p)

    problem_sizes=problem_sizes[1::5]
#     print((problem_sizes))
    
    if(summary==True):
        for r in np.arange(experiments):
            print('Problem Size: ',int(problem_sizes[r]))
            print('Iterations: ',int(iterations[r] ))
            print('average time: ',np.around(all_averages[r],decimals=4))
            print('average cost:', np.around(cost_averages[r],decimals=4) )
            print('-------------------')
            print('\n')

#  Output the stuff we really care about           
    print('Problem size: ')
    print(*problem_sizes, sep='\n')
    print('\n')
    print('Iterations: ')
    print(*iterations, sep='\n')
    print('\n')
    print('Time averages: ')
    print(*all_averages, sep='\n')
    print('\n')
    print('Cost averages: ')
    print(*cost_averages, sep='\n')
    
    # Data = {'Problem Size':  problem_sizes,
    #         'Iterations': iterations,
    #          'Time averages': all_averages,
    #         'Cost averages': cost_averages
    #         }

    # pd.options.display.float_format = '{:.4f}'.format
    # pd.set_option('display.notebook_repr_html', True)
    # df = pd.DataFrame (Data, columns = ['Problem Size','Iterations','Time averages','Cost averages'])

   

	# print(iterations)
    return [problem_sizes, iterations, all_averages, cost_averages]
# print((problem_sizes))



[ps, si, st, sc] = post_processor("../serial_implementation/out_serial.txt",5)

[ps, mai, mat, mac] = post_processor("../mpi_asynchronous/out_mpiasync_differentN.txt",5)

[ps, msi, mst, msc] = post_processor("../mpi_sync/out_mpiSync_differentN.txt",5)

[ps2, ni, nt, nc] = post_processor("../cuda_naive/out_cuda_naive.txt", 5)

[ps2, ci, ct, cc] = post_processor("../cuda_constant/out_cuda_constant.txt",5)

[ps2, shi, sht, shc] = post_processor("../cuda_shared/out_cuda_shared.txt",5)

[ps2, csi, cst, csc] = post_processor("../cuda_sync/out_cuda_sync.txt",5)



# fig = plt.figure(1)
# # We define a fake subplot that is in fact only the plot.  
# ax = fig.add_subplot(111)

# # We change the fontsize of minor ticks label 
# ax.tick_params(axis='both', which='major', labelsize=8)
# ax.tick_params(axis='both', which='minor', labelsize=8)





# plt.plot(ps, sc, label = "serial", color = 'xkcd:goldenrod')

# plt.plot(ps, mac, label = "mpi async", color = 'xkcd:fuchsia')

# plt.plot(ps, msc, label = 'mpi sync', color = 'xkcd:azure')

plt.plot(ps2, si[:len(ps2)], label = "serial", color = 'xkcd:goldenrod')

plt.plot(ps2, mai[:len(ps2)], label = "mpi async", color = 'xkcd:fuchsia')

plt.plot(ps2, msi[:len(ps2)], label = 'mpi sync', color = 'xkcd:azure')

plt.plot(ps2, ni, label = 'cuda async, constant and shared', color = 'xkcd:red')

plt.plot (ps2, csi, label = 'cuda sync', color = 'xkcd:green')

# plt.plot(ps2, ci, label = 'cuda constant', color = 'xkcd:blue')

# plt.plot (ps2, shi, label = 'cuda shared', color = 'xkcd:purple')




plt.xlabel('Problem size')
plt.ylabel('Iterations')

# show a legend on the plot
# plt.legend(bbox_to_anchor=(0.5, 0.8))
# plt.legend(bbox_to_anchor=(0.3, 0.65))
plt.legend()

# function to show the plot
# plt.figure(figsize=(5,5))
# plt.show()
# plt.rcParams['figure.figsize'] = (10,10)
plt.savefig('graph.png')


# bar graph stuff ---------------------------------

# x = [2,4,8,16,32,64]

# y1 = st[-3]/np.array(mat)
# y2 = st[-3]/np.array(mst)

# ind = np.arange(len(x))  # the x locations for the groups
# width = 0.35  # the width of the bars

# fig, ax = plt.subplots()
# rects1 = ax.bar(ind - width/2, y1, width, label='mpi async', color="xkcd:mint")
# rects2 = ax.bar(ind + width/2, y2, width, label='mpi sync', color="xkcd:violet")

# # Add some text for labels, title and custom x-axis tick labels, etc.
# ax.set_xticks(ind)
# ax.set_ylabel('Speedup')
# ax.set_xlabel('Number of processors')
# ax.set_xticklabels(('2', '4', '8', '16', '32', '64'))
# ax.legend()
# plt.savefig('graph.png')
