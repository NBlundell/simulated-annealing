import matplotlib.pyplot as plt


# x1 = [8, 64, 1024, 4096, 8192, 16384, 32768]
# x1 = [8,64,1024]
x2 = [16, 64, 128, 256, 512, 1024, 2048]
# x1 = [9, 25, 49, 81, 121]

# y1 = [0.003925,0.011117,0.196087,0.925751,2.001616,4.393449,9.218251]
# y1 = [4157,8314,13857,16628,18013,19399,20785]
y1 = [4157,8314,13857]
# plt.plot(x1, y1, label = "serial", color = 'xkcd:goldenrod')

# # y2 = [0.000737,0.007082,0.12051,0.567724,1.238856,2.882352,6.381933]
# # y2 = [2079,4157,6929,8314,9007,9700,10393]
# y2 = [2079,4157,6929]
# plt.plot(x1, y2, label = "mpi async", color = 'xkcd:fuchsia')

# # y3 = [0.037076,0.039646,0.113504,0.472838,0.937385,2.08722,4.69876]
# # y3 = [1609,1609,2326,3711,4404,5097,5790]
# y3 = [1609,1609,2326]
# plt.plot(x1, y3, label = 'mpi sync', color = 'xkcd:azure')

# y4 = [0.019919,0.113329,0.283213,0.699507,1.767196,2.913169,4.167146]
# y4 = [2302,2066,2759,3452,4145,4550,4838]
y4 = [0.010657,0.11245319979999999,0.26670020739999994,0.6580078124,1.5982870118,3.8405653808,8.856165039]
plt.plot(x2, y4, label = 'original', color = 'xkcd:crimson')

# y5 = [0.015993,0.111138,0.274657,0.687603,1.734889,2.896737,4.135668]
# y5 = [1610,1850,2540,3230,3920,4320,4610]
y5 = [0.031382,0.0712028,0.1269902,0.1879766,0.0494088,0.3360148,1.1705754]
plt.plot (x2, y5, label = 'optimised', color = 'xkcd:green')



plt.xlabel('Problem size')

plt.ylabel('Time (seconds)')

# plt.title('Serial and parallel convolution run time versus filter size')
# plt.title('Parallel convolution overhead time versus image size')

# show a legend on the plot
# plt.legend(bbox_to_anchor=(0.95, 0.8))
# plt.legend(bbox_to_anchor=(0.3, 0.65))
plt.legend()

# function to show the plot
# plt.show()
plt.savefig('graph.png')
